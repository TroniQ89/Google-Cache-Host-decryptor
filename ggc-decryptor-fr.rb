#!/usr/bin/env ruby
#encoding: UTF-8

print "GGC Decryptor v1.0\n"

class String
  # colorization
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end
  def light_blue
    colorize(36)
  end
end


serveurCacheChiffre = ARGV[0].to_s

if ARGV.empty?
 print "MERCI D'ENTRER LE NOM D'HÔTE CHIFFRÉ DE VOTRE GGC\n"
 print "Exemple: rXX--sn-YYYYYYYYY.googlevideo.com\n"
 print "Merci d'entrer le nom d'hôte encrypté, sans \"https://\"\n\n   NOM D'HÔTE?  "
 serveurCacheChiffre = STDIN.gets.chomp
end

rXX = serveurCacheChiffre[0..4].tr('-', '')

 print "\nYour GGC server is: \n\n   "
 print rXX.light_blue
 print ".".light_blue
 print serveurCacheChiffre.to_s[8..-17].tr("0-9a-z", "uzpkfa50vqlgb61wrmhc72xsnid83ytoje94").light_blue
 print ".googlevideo.com".light_blue
 print "\n\n"
 print "Merci d'utiliser ce script\n"
 print "Méthode originale trouvée par Marin: https://lafibre.info/profile/Marin\n"
 print "Réécrit de BASH à Ruby, amélioré par TroniQ89: https://lafibre.info/profile/TroniQ89\n\n"

