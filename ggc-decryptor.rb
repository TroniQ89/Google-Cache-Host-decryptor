#!/usr/bin/env ruby
#encoding: UTF-8

print "GGC Decryptor v1.0\n"

class String
  # colorization
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end
  def light_blue
    colorize(36)
  end
end


serveurCacheChiffre = ARGV[0].to_s

if ARGV.empty?
 print "PLEASE TYPE ENCRYPTED HOSTNAME OF YOUR GOOGLE GLOBAL CACHE\n"
 print "Example: rXX--sn-YYYYYYYYY.googlevideo.com\n"
 print "Please enter encrypted hostname, without \"https://\"\n\n   HOSTNAME?  "
 serveurCacheChiffre = STDIN.gets.chomp
end

rXX = serveurCacheChiffre[0..4].tr('-', '')

 print "\nYour GGC server is: \n\n   "
 print rXX.light_blue
 print ".".light_blue
 print serveurCacheChiffre.to_s[8..-17].tr("0-9a-z", "uzpkfa50vqlgb61wrmhc72xsnid83ytoje94").light_blue
 print ".googlevideo.com".light_blue
 print "\n\n"
 print "Thanks for using this script\n"
 print "Method found by Marin: https://lafibre.info/profile/Marin\n"
 print "Rewritten from BASH to Ruby, enhanced by TroniQ89: https://lafibre.info/profile/TroniQ89\n\n"

